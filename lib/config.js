const path = require('path')
const defaultsDeep = require('lodash.defaultsdeep')
const env = {
  hostname: process.env.TMPY_HOSTNAME ? process.env.TMPY_HOSTNAME : '',
  https: process.env.TMPY_HTTPS === 'true',
  port: process.env.TMPYPORT ? process.env.TMPYPORT : '8080',
  ip: process.env.TMPY_IP ? process.env.TMPY_IP : '0.0.0.0',
  'upload-dir': process.env.TMPY_UPLOAD_DIR ? process.env.TMPY_UPLOAD_DIR : path.join(__dirname, '../uploads'),
  logger: process.env.TMPY_LOGGER ? process.env.TMPY_LOGGER : 'dev',
  db: {
    host: process.env.TMPY_DB_HOST ? process.env.TMPY_DB_HOST : 'localhost',
    name: process.env.TMPY_DB_NAME ? process.env.TMPY_DB_NAME : 'tmpy',
    user: process.env.TMPY_DB_USER ? process.env.TMPY_DB_USER : 'tmpy',
    password: process.env.TMPY_DB_PASSWORD ? process.env.TMPY_DB_PASSWORD : 'tmpy'
  },
  'max-age': process.env.TMPY_MAX_AGE ? process.env.TMPY_MAX_AGE : 15,
  janitor: process.env.TMPY_JANITOR ? process.env.TMPY_JANITOR : 5
}

console.log(defaultsDeep(env))
module.exports = defaultsDeep(env)
